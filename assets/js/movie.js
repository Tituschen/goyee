
function indexMoving(){

	/*動畫開始初始化	 */
	var ct1='.boxbg01';
	var ct12=ct1 + " aside";
	var ct13=ct1 + " article h1";
	var ct14=ct1 + " article h3";

	(new TimelineLite({onComplete:initScrollAnimations}))
	.from( $(ct12), .8, {delay: .5,css:{opacity:0,y:Math.random()*50-100}})
	.from( $(ct13), .7, {/*delay: .2,*/ css:{opacity:0,x:Math.random()*100+100}})
	.from( $(ct14), .5, {/*delay: .3,*/ css:{opacity:0,x:Math.random()*100+50}})

}

	
function initScrollAnimations() {

	var controller = $.superscrollorama();

    var $wh=$(window).width();
    var offset;
    var _delay;
    var loop=false;

    $wh>=768 ? offset=-100 :  offset= -300;
    $wh>=768 ? _delay=0.15 :  _delay=0;



	var ct2='.boxbg02';
	var ct21=ct2 + " .vertical";
	var ct22=ct2 + " aside";
	var ct23=ct2 + " article h1";
	var ct24=ct2 + " article h2";

	controller.addTween(ct2, TweenMax.from( $(ct21),1, {css:{opacity:0,y:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct2, TweenMax.from( $(ct22),1.2, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct2, TweenMax.from( $(ct23),1, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct2, TweenMax.from( $(ct24),1.5, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);


	var ct3='.boxbg03';
	var ct32=ct3 + " aside";
	var ct33=ct3 + " article h1";
	var ct34=ct3 + " article h2";

	controller.addTween(ct3, TweenMax.from( $(ct32),1, {css:{opacity:0,x:Math.random()*100-300}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct3, TweenMax.from( $(ct33),1, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct3, TweenMax.from( $(ct34),1, {css:{opacity:0,x:Math.random()*100+200}, immediateRender:true}),0,offset,loop);

	var ct4='.boxbg04';
	var ct41=ct4 + " .vertical";
	var ct42=ct4 + " aside";
	var ct43=ct4 + " article h1";
	var ct44=ct4 + " article h2";
	var ct45=ct4 + " article h3";
	var ct46=ct4 + " article>a";

	controller.addTween(ct4, TweenMax.from( $(ct41),1, {css:{opacity:0,y:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct4, TweenMax.from( $(ct42),1.2, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct4, TweenMax.from( $(ct43),1, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct4, TweenMax.from( $(ct44),1, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct4, TweenMax.from( $(ct45),1.2, {css:{opacity:0,x:Math.random()*100+150}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct4, TweenMax.from( $(ct46),1, {css:{opacity:0,x:Math.random()*100+200}, immediateRender:true}),0,offset,loop);


	var ct5='.boxbg05';
	var ct51=ct5 + " .vertical";
	var ct52=ct5 + " aside";
	var ct53=ct5 + " article h1";
	var ct54=ct5 + " article h2";
	var ct56=ct5 + " article>a";

	controller.addTween(ct5, TweenMax.from( $(ct51),1, {css:{opacity:0,y:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct5, TweenMax.from( $(ct52),1.2, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct5, TweenMax.from( $(ct53),1, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct5, TweenMax.from( $(ct54),1, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct5, TweenMax.from( $(ct56),1, {css:{opacity:0,x:Math.random()*100-300}, immediateRender:true}),0,offset,loop);


	var ct6='.boxbg06';
	var ct61=ct6 + " .vertical";
	var ct62=ct6 + " aside";
	var ct63=ct6 + " article h1";
	var ct64=ct6 + " article h2";
	var ct66=ct6 + " article>a";

	controller.addTween(ct6, TweenMax.from( $(ct61),1, {css:{opacity:0,y:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct6, TweenMax.from( $(ct62),1.2, {css:{opacity:0,y:Math.random()*100+200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct6, TweenMax.from( $(ct63),1, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct6, TweenMax.from( $(ct64),1, {css:{opacity:0,x:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct6, TweenMax.from( $(ct66),1, {css:{opacity:0,x:Math.random()*100-300}, immediateRender:true}),0,offset,loop);


	var ct8='.boxbg08';
	var ct81=ct8 + " .vertical";
	var ct82=ct8 + " aside";
	var ct83=ct8 + " article h1";

	controller.addTween(ct8, TweenMax.from( $(ct81),1, {css:{opacity:0,y:Math.random()*100-200}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct8, TweenMax.from( $(ct82),1, {css:{opacity:0,y:Math.random()*100+50}, immediateRender:true}),0,offset,loop);
	controller.addTween(ct8, TweenMax.from( $(ct83),1, {css:{opacity:0,x:Math.random()*100+100}, immediateRender:true}),0,offset,loop);


	var ct9='.footertopbg';
	var ct92=ct9 + " aside";

	controller.addTween(ct9, TweenMax.from( $(ct92),1, {css:{opacity:0,y:Math.random()*100+200}, immediateRender:true}),0,offset,loop);

}


    /*
	var ct2='.comment';
	var B11='.commentList>div';

	controller.addTween(ct2, TweenMax.from( $(ct2),1.2, {css:{opacity:0,y:Math.random()*100+100}, immediateRender:true}),0,-100);

	$(B11).each(function(i) {
		controller.addTween($wh>=768 ? ct2:$(this) , TweenMax.from( $(this), 1, {
			delay: i * _delay,
			css:{
				x:Math.random()*200-100,
				y:Math.random()*400-100,
				opacity:0
			}, ease:Back.easeOut
		}),0,offset,false);
	});

	*/


//$(function).finish
