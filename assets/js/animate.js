﻿var _W=$(window).width();

//移動Function
function pageMoving(ID,offset,delay){
	var _body = (window.opera) ? (document.compatMode === "CSS1Compat" ? $('html') : $('body')) : $('html,body');
	_body.stop(false,true).delay(delay).animate({
		scrollTop:$(ID).offset().top - offset
	},500);
}


//手機選單動作
function mobileBTN(BTN,MainBtn){
	var $Block=$('.block');
	var $nav=$('.mainNav li a');

	$(BTN).click(function(e){
		$(this).toggleClass('active');
		$Block.toggleClass('active');
		$(MainBtn).toggleClass('active');
		$('.lanNav').toggleClass('active');
		e.preventDefault();
	});
	
	$Block.click(function(e){
		$Block.removeClass('active');
		$('.lanNav').removeClass('active');
		$(BTN).removeClass('active');
		$(MainBtn).removeClass('active');

		e.preventDefault();
	});	

	$nav.click(function(e){
		$Block.removeClass('active');
		$('.lanNav').removeClass('active');
		$(BTN).removeClass('active');
		$(MainBtn).removeClass('active');

		//e.preventDefault();
	});



}

//置頂Function
function btnTop(){
	$('.btnTop a').click(function(e){
		pageMoving('body',0,0);
		e.preventDefault();
	});
}


/*主選單動態*/

function mainNavActive(){
	/*
	$('a.btn-menu').mouseover(function(){
		$(this).addClass('active');
		$('.mainNav').addClass('active');
	});

	$('.mainNav').mouseleave(function(){
		$(this).removeClass('active');
		$('a.btn-menu').removeClass('active');
	});
	*/

	if(_W<768){
		$('.lanNav').removeClass('active');
	}
	
	/*languang Change*/
	$('.current-lan').mouseover(function(){
		$('.lanNav').addClass('active');
	});

	$('.lanNav').mouseleave(function(){
		$(this).removeClass('active');
	});

}


//Swiper的function
function mainSwiper(Select,Effect,Sec,pcNum,padNum,mbNum){
    var swiper = new Swiper(Select+' .swiper-container', {
      slidesPerView: pcNum,
      spaceBetween: 0,
      effect: Effect,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      
      /*autoplay: {
        delay: Sec,
        disableOnInteraction: false,
      },
     	*/
      //ipad顯示
      breakpoints: {
        768: {
          slidesPerView: padNum,
          spaceBetween: 0,
        },

         //手機顯示
        640: {
          slidesPerView: mbNum,
          spaceBetween: 0,
        },

      }
     

    });
  }

  function faqToggle(){
  	$('.faqList dl a.btn-qa-toggle').click(function(e){
  		$(this).parents('dl').addClass('active').siblings('dl').removeClass('active');
  		e.preventDefault();
  	}).first().click();
  }

  function productToggle(){
  	$('.faqList dl a.btn-qa-toggle').click(function(e){
  		$(this).parents('dl').toggleClass('active');
  		e.preventDefault();	
  	});
	  	  	  
  }

//初始化	
$(function(){
	mobileBTN('.btn-menu','.mainNav');
	btnTop();
	mainNavActive();
	$(window).on('resize',mainNavActive);

});