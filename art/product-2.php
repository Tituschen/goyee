﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv product-kv">
		<div class="container">
			<h2>GOYEE產品介紹 <small>陪伴您灌溉人生夢想</small></h2>
		</div>
	</section>
	
    <!--product-->
	<section  class="productArea">
	        <div class="container">
		        	<nav class="category variety">
	        		<ul class="inlineBlock">
	        			<li><a href="product">台幣服務</a></li>
	        			<li class="active"><a href="product-2">外幣服務</a></li>
	        			<li><a href="product-3">西聯服務</a></li>
						<li><a href="product-4">理財服務</a></li>
	        		</ul>
	        	</nav>
				
				<h1>/ 外幣服務 /</h1>

				<dl>
				   <dt><img src="../assets/images/product/icon4.png"></dt>
					<dd>
					    <h2>超優利外幣定存</h2>
					    <h3>透過數位帳戶申購美元或人民幣.即享<span>外幣超優利率</span></h3>
						<table class="tb_collapsetb">
							  <tr>
									<td>幣別</td>
									<td>優惠利率</td>
									<td>活動期間</td>
							  </tr>
							  <tr>
									<td>美金</td>
									<td>2.5%</td>
									<td>即日起-2020.12.31</td>
							  </tr>
							  <tr>
									<td>人民幣</td>
									<td>2.5%</td>
									<td>即日起-2020.12.31</td>
							  </tr>	
                         </table>
					</dd>		
				</dl>
				<dl>
				   <dt><img src="../assets/images/product/icon5.png"></dt>
				   <dd>
					   <h2>線上換匯最划算<span>最高減５分</span></h2>
					   <h3> 24小時換匯不停歇.外幣即時換</h3>
					   <table class="tb_collapsetb">
							  <tr>
									<td>幣別</td>
									<td>優惠幅度</td>
									<td>幣別</td>
									<td>優惠幅度</td>  
							  </tr>
							  <tr>
									<td>美金</td>
									<td>-3分</td>
									<td>人民幣</td>
									<td>-1分</td>  
							  </tr>
							  <tr>
									<td>日幣</td>
									<td>-0.1分</td>
									<td>英鎊</td>
									<td>-5分</td>  
							  </tr>	
						   	  <tr>
									<td>加拿大幣</td>
									<td>-2分</td>
									<td>歐元</td>
									<td>-5分</td>  
							  </tr>	
						   	  <tr>
									<td>澳幣</td>
									<td>-2分</td>
									<td>南非幣</td>
									<td>-1分</td>  
							  </tr>	
						   	  <tr>
									<td>港幣</td>
									<td>-2分</td>
									<td></td>
									<td></td>  
							  </tr>	
					   </table>
					   <p>辦理外幣兌換，依牌告匯率享優惠如表上幣別優惠，限本行營業時間09:00~15:30申購</p>
				   </dd>		
				</dl>
				
				<figure>
				  <div class="faqList display-table-xs-none">
					<dl class="">
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="text">
							<h4>注意事項</h4>
							<hr class="hr1">
							<ul>
								<li>「有效帳戶」定義為於APP申辦帳戶並審核通過</li>
								<li>「本行數位通路」定義為網路銀行、國泰世華網路銀行</li>
								<li>若客戶於本行開立之其他帳戶已享有本行其他跨行轉帳/跨行提款優惠者<br>
										※跨行提款每日回饋上限2次以「交易日」計算。</li>
								<li>舉例說明：109年4月5日(日)因遇假日，故3月份手續費計算期間將</li>
								<li>本活動僅適用於通路開立之數位存款帳戶，以下帳戶情形均不適用：<br>
									<ol>
										<li>(1)透過本行官網開立之數位存款帳戶。</li>
										<li>(2)透過國泰證券開戶APP開立之數位存款帳戶。</li>
										<li>(3)透過通路開立之數位存款帳戶，惟已至任一分行臨櫃轉換為一般活期。</li>
									</ol>
								<li>帳戶所提供之跨行轉帳/跨行提款手續費優惠，以滿足客戶日常生活</li>
								<li>專案期間倘因不可歸責於本行之事由，本行得變更或終止內容，官網公告。</li>
								<li>若有其他未盡事宜，悉依本行相關規定或解釋辦理。若有任何疑問。</li>
								<li>透過本行官網開立之數位存款帳戶。</li>
								<li>本活動僅適用於通路開立之數位存款帳戶，以下帳戶情形均不適用</li>
                            </ul>
                            <hr>
						</dd>
					</dl>	
				</figure>
				
		     </div>
	</section>
	
	

</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		productToggle();
  	})
</script>

</body>
</html>

