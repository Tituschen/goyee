﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li >
					<h4>01.</h4>
					<p>上傳您的資料</p>
				</li>				

				<li >
					<h4>02.</h4>
					<p>填寫個人資料</p>
				</li>				

				<li class="active">
					<h4>03.</h4>
					<p>填寫申請資料</p>
				</li>				

				<li>
					<h4>04</h4>
					<p>確認資料</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document form">
	        <div class="container">

	            <h1>請填寫您的申請資料</h1>
				
				<div class="row formList">

					<div class="col-sm-3 img">
						<h2 class="org text-right">申請資料</h2>
				    </div>

				    <div class="col-sm-6 txt">
				    	<ul>

				    		<li>
				    			<h5>行業別</h5>
				    			<div class="clip clip-2">
					    			<select><option>分類</option></select>
					    			<select><option>項目</option></select>
				    			</div>
				    		</li>						    		

				    		<li>
				    			<h5>職稱</h5>
					    		<select>
					    			<option>請選擇</option>
					    		</select>
				    		</li>

				    		<li>
				    			<h5>開戶目的</h5>
				    			<select>
				    				<option>請選擇</option>
				    				<option>儲蓄</option>
									<option>薪轉</option>
									<option>證券戶</option>
									<option>資金調撥</option>
									<option>投資理財</option>
									<option>貸款撥入</option>
									<option>其他</option>
				    			</select>
				    		</li>	

				    		<li>
				    			<h5>公司名稱</h5>
				    			<input type="text"  placeholder="請輸入公司中文名稱" name="">
				    		</li>						    		

				    		<li>
				    			<h5>年收入</h5>
				    			<select>
				    				<option>50萬以下</option>
				    				<option>51~100萬</option>
									<option>101~300萬</option>
									<option>301~500萬</option>
									<option>501~1000萬</option>
									<option>1001萬以上</option>
				    			</select>
				    		</li>		

				    		<li>
				    			<h5>區碼</h5>
				    			<input type="text"  placeholder="請輸入公司電話區碼" name="">
				    			<h5>公司電話</h5>
				    			<input type="text"  placeholder="請輸入公司電話" name="">
				    			<h5>分機</h5>
				    			<input type="text"  placeholder="請輸入公司電話分機" name="">

				    		</li>						    		

				    		<li>
				    			<h5>他行帳戶驗證</h5>
				    			<input type="text"  placeholder="請選擇驗證銀行" name="">
				    			<input type="text"  placeholder="請輸入帳號" name="">
				    			<p>顯示方式： 銀行代碼+銀行名稱（例：054 京城銀行）</p>
				    		</li>				    		

				    	</ul>
				    </div>
				</div>

				<hr>

				<div class="agreement text-center">
					<h5>貴行基於提供特定優惠、服務或行銷之目的，將本人(含法定代理人、 輔助人)基本資料提供予貴行及貴行之各關係企業(京城銀國際租賃(股)公司、京城國際建築經理(股)公司， 日後若有新增或異動，將揭露於貴行全球資訊網)、貴行合作推廣之單位為行銷建檔、揭露、轉介或交互運用。</h5>

					<label>
    					<input type="radio" name="aa" checked=""><span>是</span>
    					<input type="radio" name="aa"><span>否</span>
    				</label>
				</div>


				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="establishment-5">上一步</a></div>
					<div class="col-6"><a class="btn-send" href="establishment-7">下一步</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

