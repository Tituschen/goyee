﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li >
					<h4>01.</h4>
					<p>身分確認</p>
				</li>				

				<li >
					<h4>02.</h4>
					<p>OTP驗證</p>
				</li>				

				<li class="active">
					<h4>03.</h4>
					<p>設定使用者代碼</p>
				</li>				

				<li>
					<h4>04</h4>
					<p>開通成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
		<section class="document webbank">
        <div class="container">
            <h1>啟動數位新生活</h1>
			<article>
			  <ul>				  
				  <li><input type="text" placeholder="請輸入您的使用者代碼" ></li>
				  <li><input type="text" placeholder="請再輸入一次使用者代碼" ></li>
				  <li><input type="password" placeholder="請輸入您的使用者密碼" ></li>
				  <li><input type="password" placeholder="請再輸入一次使用者密碼" ></li>
			  </ul>
			  <hr>
			  <ol class="caution">
			  	<li>使用者代碼長度6~10碼，網銀密碼長度6~8碼</li>
				<li>兩者皆須使用數字及英文字母
				    (不區分大小寫)混和組成</li>
				<li>不得使用空白鍵及各種非英數字符號</li>
				<li>使用者代碼及網銀密碼不可相同。</li>
			  </ol>	
			</article>

			<div class="row  btns">
			    <div class="col-6"><a class="btn-send" href="webbank-open">取消</a></div>
				<div class="col-6"><a class="btn-send" href="webbank-open-5">下一步</a></div>
			</div>
	    </div><!--containerEND-->
		</section>		
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>


</body>
</html>

