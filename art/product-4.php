﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv product-kv">
		<div class="container">
			<h2>GOYEE產品介紹 <small>陪伴您灌溉人生夢想</small></h2>
		</div>
	</section>
	
    <!--product-->
	<section  class="productArea">
	        <div class="container">
		        	<nav class="category variety">
	        		<ul class="inlineBlock">
	        			<li><a href="product">台幣服務</a></li>
	        			<!--<li><a href="product-2">外幣服務</a></li>-->
	        			<li><a href="product-3">西聯服務</a></li>
						<li class="active"><a href="product-4">理財服務</a></li>
	        		</ul>
	        	</nav>
				
				<h1>理財服務</h1>
				<dl hidden="">
				   <dt><img src="../assets/images/product/icon8.png"></dt>
				   <dd>
					   <h1>輕鬆投資 GoYee Select隨意選</h1>
					   <h3>沒想法嗎，讓GoYee的理財團隊<br>幫你建議專業的投資組合詳細內容請參考注意事項</h3>
				   </dd>		
				</dl>
				
				<dl>
				   <dt><img src="../assets/images/product/icon9.png"></dt>
					<dd>
					    <h2>京選基金<span>手續費4折優惠</span></h2>
					    <h3>搭配手續費優惠，讓投資收益最大化<br>
					    		京選基金請參考注意事項
					    </h3>
						<!--<h3>賺多少拿多少!</h3>-->
					</dd>		
				</dl>
				
				<dl>
				  <dt><img src="../assets/images/product/icon10.png"></dt>
				   <dd>
					   <h1>線上快速KYC</h1>
					   <h3>快速了解你的風險級別，<br>挑選最適合自己的產品<br></h3>
					   <p>※投資一定有風險，基金投資有賺有賠，申購前應詳閱公開說明書。</p>
				   </dd>		
				</dl>

				
				<figure>
				  <div class="faqList display-table-xs-none">
					<dl class="">
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="text">
							<h4>注意事項</h4>
							<hr class="hr1">
							<ul>
								<li>專案期間：109年3月2日至109年5月31日止</li>
								<li>優惠內容：於專案期間內，客戶網路銀行或行動銀行APP單筆及定期定額分別申購下列京選基金即可享手續費4折優惠。</li>
								<li>京選基金列表：
									<table class="table table-bordered">
									  <tr>
									    <th >基金類型</th>
									    <th>基金商品名稱</th>
									    <th >基金代碼</th>
									  </tr>
									  <tr>
									    <td  rowspan="6">單筆申購</td>
									    <td>摩根投資基金－JPM多重收益(美元對沖累計)</td>
									    <td >00010130</td>
									  </tr>
									  <tr>
									    <td>摩根投資基金－JPM多重收益(美元對沖月配現金)</td>
									    <td >00010129</td>
									  </tr>
									  <tr>
									    <td>安聯收益成長基金－ＡＴ累積類股(美元)</td>
									    <td >00030025</td>
									  </tr>
									  <tr>
									    <td>安聯收益成長基金－ＡＭ穩定月收類股(美元月配現金)</td>
									    <td >00410001</td>
									  </tr>
									  <tr>
									    <td>富達全球多重資產收益基金(Ａ股累計美元) </td>
									    <td >0004GF97</td>
									  </tr>
									  <tr>
									    <td>富達全球多重資產收益基金(Ｆ１穩定月配Ａ股美元) </td>
									    <td >0004GF98</td>
									  </tr>
									  <tr>
									    <td  rowspan="3">定期定額</td>
									    <td>安聯台灣大壩Ａ類台幣 </td>
									    <td >10020001</td>
									  </tr>
									  <tr>
									    <td>保德信中國品牌基金 </td>
									    <td >10060031</td>
									  </tr>
									  <tr>
									    <td>摩根金－JPM美國科技(美元)－A股(分派) </td>
									  <td >00010140</td>
									  </tr>
									</table>
								</li>
                            </ul>
                            <hr>
						</dd>
					</dl>	
				</figure>
				
		     </div>
	</section>
	

</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		productToggle();
  	})
</script>

</body>
</html>

