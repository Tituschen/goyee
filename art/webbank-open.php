﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv webbank-kv">
		<div class="container">
			<h2>網銀開通<small>四個步驟，一次滿足，三種需求</small></h2>
		</div>
	</section>
	
	<!--Interrogation-->
	<section class="Interrogation webbankInter">
	        <div class="container">
			  <h1><b>Welcome to GOYEE!</b></h1>
			  <h2>我想要...</h2>
				<ul class="inlineBlock">
				   <li>
				   		<a href="establishment">
						   <aside><img src="../assets/images/webbank/img1.png" alt=""/></aside>
						   <h4>加入京城數位帳戶</h4>
						   <p>我還沒有京城銀行的<br>帳戶，想要申請京城<br>數位帳戶。</p>
						</a>
				   </li>
				   <li>
				   		<a href="webbank-open-2">
						   <aside class="active"><img src="../assets/images/webbank/img2.png" alt=""/></aside>
						   <h4>啟用京城數位帳戶</h4>
						    <p>我要啟用行動銀行，<br>開始使用數位服務。</p>
						</a>
				   </li>
				   <li>
				   		<a href="webbank-open-2">
						   <aside><img src="../assets/images/webbank/img3.png" alt=""/></aside>
						   <h4>已有京城帳戶</h4>
						    <p>我已經是京城銀行<br>客戶，但是尚未開通網<br>路銀行服務。</p>
						</a>
				   </li>		
				</ul>
	        </div><!--containerEND-->
	</section>
	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){

  	})
</script>


</body>
</html>

