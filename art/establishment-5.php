﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li >
					<h4>01.</h4>
					<p>上傳您的資料</p>
				</li>				

				<li class="active">
					<h4>02.</h4>
					<p>填寫個人資料</p>
				</li>				

				<li>
					<h4>03.</h4>
					<p>填寫申請資料</p>
				</li>				

				<li>
					<h4>04</h4>
					<p>確認資料</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document form">
	        <div class="container">

	            <h1>請填寫您的個人資料</h1>
				
				<div class="row formList">

					<div class="col-sm-3 img">
						<h2 class="org text-right">個人資料</h2>
				    </div>

				    <div class="col-sm-6 txt">
				    	<ul>

				    		<li>
				    			<h5>手機號碼</h5>
				    			<input type="text"  placeholder="請輸入" name="" value="0988777666" disabled="">
				    		</li>		

				    		<li>
				    			<h5>Email</h5>
				    			<input type="text"  placeholder="請輸入Email" name="">
				    		</li>		


				    		<li>
				    			<h5>中文姓名</h5>
				    			<input type="text"  placeholder="請輸入" name="" value="王大明" disabled="">
				    		</li>						    		

				    		<li>
				    			<h5>英文姓名（與護照相同）</h5>
				    			<input type="text"  placeholder="請輸入" name="" style="margin-bottom:10px;">
				    			<button class="orgBtn">幫我翻英文</button>
				    		</li>				    		

				    	</ul>
				    </div>
				</div>


				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="establishment-4">上一步</a></div>
					<div class="col-6"><a class="btn-send" href="establishment-6">下一步</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

