﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">

	<!--newsSwiper-->
	<section class="newsSwiper">

		<div class="swiperArea bn-swiper">
			<div class="swiper-container">

			    <div class="swiper-wrapper">

			      <div class="swiper-slide">
				      	<a href="index"><aside><img src="../assets/images/banner/banner.jpg"></aside></a>
			  	  </div>					      

			    </div><!--SwiperWrapper-end-->

			    <!-- Add Pagination -->
			    <div class="swiper-pagination" hidden=""></div>

			    <!-- Add Arrows 
			    	<div class="swiper-button-next "></div>
			    	<div class="swiper-button-prev "></div>
			    -->

  			</div><!--SwiperContainer-end-->
	  	</div>


     </section> 
	
	<!--news-->
	<section class="newsArea">
	        <div class="container">
	        	<nav class="category">
	        		<ul class="inlineBlock">
	        			<li class="active"><a href="#">ALL</a></li>
	        			<li><a href="#">最新消息</a></li>
	        			<li><a href="#">重要公告</a></li>
	        		</ul>
	        	</nav>

				<div class="newsList display-table display-table-xs-none">

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>最新消息</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>重要公告</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>					

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>最新消息</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>重要公告</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 </p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>最新消息</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>重要公告</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>					

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>最新消息</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>重要公告</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 </p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>					

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>最新消息</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup</p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>						

					<dl>
						<dt><date>2019.10.21</date></dt>
						<dd class="cate"><span>重要公告</span></dd>
						<dd class="text"><p class="txtOver2">年末年始休業期間 2019 Campaign Roundup年末年始休業期間 2019 </p></dd>
						<dd class="more"><a href="news-content">read more</a></dd>
					</dl>					

				</div>

				<!--pagination-->
				<nav aria-label="Page navigation">
				  <ul class="pagination justify-content-center">
				    <li class="page-item">
				    	<a class="page-link page-link-prev" href="#"><img src="../assets/images/ico/arrow-left.png">Prev</a>
				    </li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item"><a class="page-link" href="#">4</a></li>
				    <li class="page-item"><a class="page-link" href="#">5</a></li>
				    <li class="page-item">
				    	<a class="page-link  page-link-next" href="#">Next<img src="../assets/images/ico/arrow-left.png"></a>
				    </li>
				  </ul>
				</nav>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		mainSwiper('.bn-swiper','slider',3000,1,1,1);
  	})
</script>

</body>
</html>

