<!--pc header-->
<header>
  <div class="container">
		<div class="row">

			<div class="col-5 col-sm-3">
				<h1 class="logo">
					<a href="./index"><img src="../assets/images/logo.png"></a>
				</h1>
			</div>

			<div class="col-7 col-sm-9 text-right">

				<nav id="Nav" class="mainNav ">
					<ul class="inlineBlock">
						<li><a href="news">最新消息</a></li>
						<li><a href="product">產品介紹</a></li>
						<li><a href="debit-open">服務開通</a></li>
						<li><a href="faq">FAQ</a></li>               
					</ul>
					<a href="#" class="btn-menu"><hr><hr></a>
				</nav>

				<a class="openingBtn cardBtn" href="webbank-open" hidden="">開卡&開通</a>
				<a class="openingBtn" href="establishment">立即開戶</a>

			</div>

			<div class="col-sm-12 text-right">

				<nav class="lanNav active">
					<ul class="inlineBlock">
						<li><a href="#">VN</a></li>
						<li><a href="#">ID</a></li>
						<li><a href="#">EN</a></li>
						<li><a href="#">中</a></li>
					</ul>	 
					<button class="current-lan">EN</button>
		 		</nav>

			</div>

		</div><!--rowEnd-->

 	 </div><!--containerEnd-->
</header>

<!--block-->
<div class="block"></div>
	
