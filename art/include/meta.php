<?php 
	$time ="?".date ("Ymd-Hi"); 
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<title>Goyee</title>

   
<!--ICO-->
<link rel="Shortcut Icon" type="image/x-icon" href="../assets/images/favicon.ico" />

<!--LibraryClass-->
<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/swiper.min.css" rel="stylesheet">

<!--My Css-->
<link href="../assets/css/style.css<?=$time?>" rel="stylesheet">
<link href="../assets/css/layout.css<?=$time?>" rel="stylesheet">
<link href="../assets/css/main.css<?=$time?>" rel="stylesheet">
<link href="../assets/css/mobileNav.css<?=$time?>" rel="stylesheet">
<link href="../assets/css/rwd.css<?=$time?>" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.min.js"></script>
      <script src="../assets/js/respond.min.js"></script>
<![endif]-->