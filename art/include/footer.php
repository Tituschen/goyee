
<!--Footernav-->	
<div class="footertop">
	<div class="container">
        <div class="row align-items-center">
			<div class="col-sm-6">
			  <article>	
				   <h1>立即下載</h1>
				   <h6>完成ＧＯＹＥＥ開戶了嗎？ 立即下載ＡＰＰ</h6>	
			  </article>	  
			</div>
		    <div class="col-sm-6">
			   <ul class="inlineBlock">
				  <li><img src="../assets/images/QR.png" alt=""/></li> 
				  <li><img src="../assets/images/app_logo.png" alt=""/></li>
			   </ul>
			</div>
		</div>
    </div><!--containerEND-->
</div>	

<!--Footernav-->	
<div class="footerBottom hidden">
	<div class="container">
		<nav class="footerNav">
			<ul class="inlineBlock">
				<li>
					<h3>關於京城銀行</h3>
					<a href="#">組織架構</a>
					<a href="#">經營團隊</a>
					<a href="#">服務據點</a>
					<a href="#">大事紀</a>
					<a href="#">集團介紹</a>
					<a href="#">人才招募</a>
				</li>								
				<li>
					<h3>關於京城銀行</h3>
					<a href="#">組織架構</a>
					<a href="#">經營團隊</a>
					<a href="#">服務據點</a>
					<a href="#">大事紀</a>
					<a href="#">集團介紹</a>
					<a href="#">人才招募</a>
				</li>	
				<li>
					<h3>關於京城銀行</h3>
					<a href="#">組織架構</a>
					<a href="#">經營團隊</a>
					<a href="#">服務據點</a>
					<a href="#">大事紀</a>
					<a href="#">集團介紹</a>
					<a href="#">人才招募</a>
				</li>	
				<li>
					<h3>關於京城銀行</h3>
					<a href="#">組織架構</a>
					<a href="#">經營團隊</a>
					<a href="#">服務據點</a>
					<a href="#">大事紀</a>
					<a href="#">集團介紹</a>
					<a href="#">人才招募</a>
				</li>	
			</ul>
	   </nav>
    </div><!--containerEND-->
</div>	
	
<!--Footer-->
<footer>
	<div class="container">	
       <ul class="inlineBlock">
		  <li>台南市中西區西門路一段506號</li>
		  <li>TEL:06-213-1259</li>
		  <li> FAX:06-213-1335</li> 
		  <li><a href="https://customer.ktb.com.tw/new/faq/?class_slug=10488eb8" target="_blank">隱私權保護政策</a></li>
		  <li><a href="https://customer.ktb.com.tw/new/faq/?class_slug=e97f38d7" target="_blank">瀏覽器建議</a></li>
		  <li><a href="https://customer.ktb.com.tw/new/faq/?class_slug=afb3d5cc" target="_blank">個人資料告知事項</a></li>
	   </ul>
	   <p>©京城商業銀行版權所有</p>	
  </div><!--containerEND-->
</footer>


<!--btnTop-->
<nav class="btnTop">
	<a href="#" class="flex-center">
		<img class="whiten" src="../assets/images/ico/arrow.png">
	</a>
</nav>

