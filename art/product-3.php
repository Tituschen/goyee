﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv product-kv">
		<div class="container">
			<h2>GOYEE產品介紹 <small>陪伴您灌溉人生夢想</small></h2>
		</div>
	</section>
	
    <!--product-->
	<section  class="productArea">
	        <div class="container">
		        	<nav class="category variety">
	        		<ul class="inlineBlock">
	        			<li><a href="product">台幣服務</a></li>
	        			<!--<li><a href="product-2">外幣服務</a></li>-->
	        			<li class="active"><a href="product-3">西聯服務</a></li>
						<li><a href="product-4">理財服務</a></li>
	        		</ul>
	        	</nav>
				
				<h1>西聯服務</h1>
				<dl>
				   <dt><img src="../assets/images/product/icon6.png"></dt>
				   <dd>
					   <h1>網路西聯匯款 快速又便利</h1>
					   <h2><span>24小時全年無休的服務</span></h2>
					   <h3>手續簡便收款快速收款免付手續費</h3>
				   </dd>		
				</dl>
				<dl>
				    <dt><img src="../assets/images/product/icon7.png"></dt>
					<dd>
					    <h2><span>免出門 在家立即收款</span></h2>
					    <h3>開戶完成，立即收款，免手續費，Google AdSense廣告收入、airbnb租金收入的第一選擇</h3>
					</dd>		
				</dl>

				
				<figure>
				  <div class="faqList display-table-xs-none">
					<dl class="">
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="text">
							<h4>注意事項</h4>
							<hr class="hr1">
							<ul>
								<li>網路西聯匯入匯款入帳金額以新台幣為主，兌換匯率適用網頁公告之｢國際快捷匯款USD｣匯率，如欲存入外幣帳戶，將依「京城銀行西聯匯款匯率適用標準」收取匯差。</li>
								<li>進行網路西聯匯款之各項匯款交易時，將逐筆如實申報結匯金額及匯款性質，倘若發生申報不實或填寫不正確之情事，其後果由立約人自行負責。</li>
								<li>網路西聯匯款之各項交易，應遵照中央銀行公布之「外匯收支或交易申報辦法」、「銀行業輔導客戶申報外匯收支或交易應注意事項」辦理。</li>
								<li>每人每筆及每交易日之交易（非營業時間交易合併計入次營業日）涉及新臺幣兌換之交易限額為等值新臺幣五十萬元（不含）以下。超過限額或異常之外匯交易，請臨櫃辦理。</li>
                            </ul>
                            <hr>
						</dd>
					</dl>	
				</figure>
				
		     </div>
	</section>
	
	

</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		productToggle();
  	})
</script>

</body>
</html>

