﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--stepList-->
    <div class="stepList debit-step">
		<div class="container">
			<ul class="inlineBlock">
				<li>
					<h4>01.</h4>
					<p>登入</p>
				</li>				

				<li >
					<h4>02.</h4>
					<p>OTP驗證</p>
				</li>							

				<li class="active">
					<h4>03.</h4>
					<p>開卡成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document debit">
	        <div class="container">
	            <h1>啟動數位新生活</h1>
				<article>

		            <h3 class="text-center">
		            	您已完成金融卡開卡，請至全國任一台ATM或本行
		            	<a href="https://eatm.ktb.com.tw/KTBWebATM/index.jsp" target="_blank" class="underline">eATM</a>進行提款密碼變更，提款預設密碼將寄送至您的e-mail信箱，即可開始體驗GOYEE給您帶來的全方位數位服務!

		        	</h3>
		            <hr>
					
					<div class="row align-items-center webbank-final ">
						<div class="col-6 text-center hidden">
							<img src="../assets/images/webbank/map.png">
							<h4>服務據點</h4>
						</div>						

						<div class="col-6 text-center offset-3">
							<img src="../assets/images/debit/qr-atm.jpg" style="height:150px">
						</div>

					</div>

					<hr>
					
	            </article>

				<div class="row  btns">
					<div class="col-6  text-center">
						<a class="btn-send" href="index">回首頁</a>
					</div>					
					<div class="col-6  text-center">
						<a class="btn-send" href="#">未收到密碼函</a>
					</div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>


</body>
</html>

