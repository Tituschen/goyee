﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
	<!--news-->
	<section class="newsArea">
	        <div class="container">

	        	<nav class="category">
	        		<ul class="inlineBlock">
	        			<li class="active"><a href="#">ALL</a></li>
	        			<li><a href="#">最新消息</a></li>
	        			<li><a href="#">重要公告</a></li>
	        		</ul>
	        	</nav>

				<div class="newsContent">
					<date>2019.10.21</date>
					<span class="category-ico">最新消息</span>
					<h2 class="news-title">周報事例-2019 Campaign Roundup</h2>
					
					<!--文字編輯器-->
					<article class="editZone">
						<p>辦理外幣兌換，依牌告匯率享優惠如表上幣別優惠，若曾成功參加任何與Richart用戶禮相關之行銷推廣活動，<br>
						即無法參加本活動；已成功參加本活動【被推薦人】者，<br>
						亦不得再參加即無法綁定被推薦人邀請序號。
						</p>
						
						<p>收受簡訊後請於2020/06/30前自行登入LINE POINT CODE網站<br>
						（網址：<a href="https://points.line.me/pointcode">https://points.line.me/pointcode</a>）進行儲值及兌換使用。
						</p>
					</article>
					<!--文字編輯器End-->

				</div>

				<!--pagination-->
				<nav aria-label="Page navigation">
				  <ul class="pagination justify-content-center">
				    <li class="page-item">
				    	<a class="page-link page-link-prev" href="#"><img src="../assets/images/ico/arrow-left.png">上一則</a>
				    </li>
				    <li class="page-item page-link-back"><a class="page-link" href="news">Back</a></li>
				    <li class="page-item">
				    	<a class="page-link  page-link-next" href="#">下一則<img src="../assets/images/ico/arrow-left.png"></a>
				    </li>
				  </ul>
				</nav>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

