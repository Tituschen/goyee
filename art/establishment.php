﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv etckv">
		<div class="container">
			<h2>GOYEE <small>數位帳戶開戶</small></h2>
		</div>
	</section>
	
	<!--Interrogation-->
	<section class="Interrogation establishment">
	        <div class="container">
			  <h1><b>Welcome to GOYEE!</b></h1>
			  <h2>準備好開戶了嗎？ 您是否備妥下列文件</h2>
				<ul class="inlineBlock">
				   <li>
					   <aside><img src="../assets/images/securities01.png" alt=""/></aside>
					   <h4>雙證件</h4>
				   </li>
				   <li>
					   <aside><img src="../assets/images/securities02.png" alt=""/></aside>
					   <h4>他行帳號/存摺</h4>
				   </li>
				   <li>
					   <aside><img src="../assets/images/securities03.png" alt=""/></aside>
					   <h4>自然人憑證</h4>
				   </li>		
				</ul>
	        </div><!--containerEND-->
	</section>


	<!--Information-->
	<section class="Information">
	        <div class="container">
				<h1>年滿20歲之本國人、身分證、第二證件、他行帳號</h1>
				<article>
				   <h3>若您要使用您的他行帳號進行驗證,帳號限制如下:</h3>
				   <ul>
					   <li>請使用您的同名帳號</li>
					   <li>行帳號需以臨櫃方式開立</li>
					   <li>第二證件僅限本國健保卡、駕照</li>
					   <li>目前可驗證的銀行：
					      <ol class="inlineBlock">
							<li>004 臺灣銀行</li>
							<li>007 第一銀行</li>
							<li>008 華南商業銀行</li>
							<li>009 彰化商業銀行</li>
							<li>011 上海商業儲蓄銀行</li>
							<li>012 台北富邦商業銀行</li>
							<li>013 國泰世華商業銀行</li>
							<li>016 高雄銀行</li>
							<li>017 兆豐國際商業銀行</li>
							<li>021 花旗(台灣)商業銀行</li>
							<li>048 王道商業銀行</li>
							<li>052 渣打國際商業銀行</li>
							<li>081 滙豐(台灣)商業銀行</li>
							<li>101 瑞興商業銀行</li>
							<li>102 華泰商業銀行</li>
							<li>103 臺灣新光商業銀行</li>
							<li>108 陽信商業銀行</li>
							<li>118 板信商業銀行</li>
							<li>146 台中市第二信用合作社</li>
							<li>162 彰化第六信用合作社</li>			  
							<li>216 花蓮第二信用合作社</li>						      
							<li>600 農金資訊股份有限公司</li>						      
							<li>803 聯邦商業銀行</li>
							<li>805 遠東國際商業銀行</li>
							<li>806 元大商業銀行</li>
							<li>807 永豐商業銀行</li>
							<li>808 玉山商業銀行</li>
							<li>809 凱基商業銀行</li>
							<li>810 星展(台灣)商業銀行</li>
							<li>812 台新國際商業銀行</li>
							<li>815 日盛國際商業銀行</li>
							<li>816 安泰商業銀行</li>
							<li>822 中國信託商業銀行</li>
							<li>952 財團法人農漁會南區資訊中心</li> 
						  </ol>
					   </li>
				   </ul>	
				</article>

				<div class="btns text-center">
	            	<a class="btn-send" href="establishment-2">準備好了</a>
	       		 </div>
	        </div><!--containerEND-->
	</section>
	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){

  	})
</script>


</body>
</html>

