﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li >
					<h4>01.</h4>
					<p>身分確認</p>
				</li>				

				<li>
					<h4>02.</h4>
					<p>OTP驗證</p>
				</li>				

				<li>
					<h4>03.</h4>
					<p>設定使用者代碼</p>
				</li>				

				<li class="active">
					<h4>04</h4>
					<p>開通成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
		<section class="document webbank">
	        <div class="container">

				<article>

		            <h1>啟動數位新生活</h1>
		            <h3 class="text-center">您已完成服務開通，<br>再差一步就可以使用數位服務，</h3>
		            <h3 class="org"><b>掃描Qrcode或點擊下載京城行動銀行!</b></h3>
		            <hr>
					
					<div class="row align-items-center webbank-final ">
						<div class="col-6 text-center hidden">
							<img src="../assets/images/webbank/map.png">
							<h4>服務據點</h4>
						</div>						

						<div class="col-6 offset-3 text-center">
							<img src="../assets/images/webbank/qr.png" style="height:150px">
						</div>

					</div>

					<hr>
					
	            </article>	
				
				<div class="row  btns">
					<div class="col-6 offset-3 text-center">
						<a class="btn-send" href="index">回首頁</a>
					</div>					
				</div>

		    </div><!--containerEND-->
		</section>		
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){

  	});
</script>


</body>
</html>

