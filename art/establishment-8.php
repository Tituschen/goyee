﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body class="all-item">
<?php include("include/header.php"); ?>
			
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv etckv">
		<div class="container">
			<h2>KTB<br>數位帳戶開戶</h2>
		</div>
	</section>
	
		<!--document-->
	<section class="document endbox">

        <div class="container">
            
            <h1>您已成功申請GOYEE數位帳戶!!</h1>
			<article>
                <h2>請至您的email信箱進行驗證</h2>
				<h2>本行將於5個營業日內，通知您開戶結果</h2>
			</article>

			<div class="row  btns">
				<div class="col-6 offset-3 text-center">
					<a class="btn-send" href="index">回首頁</a>
				</div>
			</div>	
	
	 	</div><!--containerEND-->
	</section>

</main>
<!--主版位End-->

	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>


<script>
  	$(function(){})
</script>


</body>
</html>

