﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li class="active">
					<h4>01.</h4>
					<p>身分確認</p>
				</li>				

				<li >
					<h4>02.</h4>
					<p>OTP驗證</p>
				</li>				

				<li>
					<h4>03.</h4>
					<p>設定使用者代碼</p>
				</li>				

				<li>
					<h4>04</h4>
					<p>開通成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document webbank">
	        <div class="container">
	            <h1>啟動數位新生活</h1>
				<article>
				  <ul>				  
					  <li><h3>請輸入您的身分證字號</h3></li>
					  <li><input type="text" placeholder="請輸入身分證字號"></li>
					  <li><h3>請輸入您的生日</h3></li>
					  <li><input type="text" placeholder="請輸入您的西元出生年月日，共八碼"></li>
                      <li><h3>輸入檢核碼</h3></li>

					  <li>
					  	<div class="clip clip-3 verifyArea">
					  		<input type="text" placeholder="請輸入檢核碼">
					  		<img class="verifyImg" src="../assets/images/5566.png" alt="">
					  		<h6 class="text-center">
					  			<a href="#"><img src="../assets/images/icon02.png" alt=""/></a>刷新驗證碼
					  		</h6>
					 	</div>
				  </ul>
				  <hr>
				</article>

				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="webbank-open">取消</a></div>
					<div class="col-6"><a class="btn-send" href="webbank-open-3">下一步</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>


</body>
</html>

