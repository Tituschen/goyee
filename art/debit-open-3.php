﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--stepList-->
    <div class="stepList debit-step">
		<div class="container">
			<ul class="inlineBlock">
				<li>
					<h4>01.</h4>
					<p>登入</p>
				</li>				

				<li class="active">
					<h4>02.</h4>
					<p>OTP驗證</p>
				</li>							

				<li>
					<h4>03.</h4>
					<p>開卡成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document testbox debit">
	        <div class="container">
	            <h1>啟動數位新生活</h1>
				<article>
				  <ul>				  
					  <li><h3>簡訊驗證碼已於 10:27 發送至</h3></li>
					  <li>
						  <input type="text" placeholder="0988***666" v-bind:disabled="isReadOnly">
					      <p>有效時間剩餘<b>60</b>秒</p>
					  </li>
	                  <li><h3>輸入檢核碼</h3></li>
					  <li>
						  <input type="text" placeholder="請輸入6位驗證碼" v-bind:disabled="isReadOnly">
					      <p><a href="#">我沒有收到簡訊</a></p>
					  </li>
				  </ul>
				  <hr>	
				</article>

				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="debit-open-2">上一步</a></div>
					<div class="col-6"><a class="btn-send" href="debit-open-4">下一步</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>


</body>
</html>

