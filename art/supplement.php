﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList stepList-3">
		<div class="container">
			<ul class="inlineBlock">
				<li class="active">
					<h4>01.</h4>
					<p>更新資料</p>
				</li>										

				<li>
					<h4>02.</h4>
					<p>確認資料</p>
				</li>

				<li>
					<h4>03.</h4>
					<p>修改成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document form">
	        <div class="container">

	            <h1>/ 請重新上傳文件 /</h1>
				
				<div class="row formList">

					<div class="col-sm-6 offset-md-3 img">
						<h3 class="org">Step1：上傳身分證照片（正反面）</h3>
						<p>請上傳您的身分證照片正反面</p>

						<aside>
							<input type="file" name="">
							<img src="../assets/images/id/1.png">
						</aside>
						
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
				    </div>

				</div>

				<hr>

				<div class="row formList">

					<div class="col-sm-6 offset-md-3 img">
						<aside>
							<input type="file" name="">
							<img src="../assets/images/id/2.png">
						</aside>
						
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
				    </div>

				</div>

				<hr>

				<div class="row formList">

					<div class="col-md-6 offset-md-3  img">

						<h3 class="org">Step2：上傳第二身分證照片</h3>
						<p>上傳可辨識您身分的第二證件　例如：健保卡．駕照</p>
						<aside>
							<input type="file" name="">
							<img src="../assets/images/id/3.png">
						</aside>
						
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
				    </div>

				</div>

				<hr>

				<h3 class="org supplemtnt-h3">Step3：確認資料正確</h3>
				<div class="formListCheck display-table">

					<dl>
						<dt>中文姓名:</dt>
						<dd><input type="text" value="宋喬安"></dd>
					</dl>					

					<dl>
						<dt>身分證字號:</dt>
						<dd><input type="text" value="A223456789" disabled=""></dd>
					</dl>					


					<dl>
						<dt>手機號碼:</dt>
						<dd><input type="text" value="0988777666" disabled=""></dd>
					</dl>					

					<dl>
						<dt>出生日期:</dt>
						<dd><input type="text" value="69/5/23"></dd>
					</dl>					

					<dl>
						<dt>發證日期:</dt>
						<dd><input type="text" value="100/12/31"></dd>
					</dl>
				</div>				

				<div class="formListCheck display-table">
					<dl>
						<dt>英文姓名:</dt>
						<dd><input type="text" value="SUNG CHIAO AN"></dd>
					</dl>					

					<dl>
						<dt>開戶目的:</dt>
						<dd>
							<select>
				    			<option>請投資理財</option>
				    		</select>
				    	</dd>
					</dl>					

					<dl>
						<dt>行業:</dt>
						<dd>
				    		<div class="clip clip-2">
				    			<select><option>分類</option></select>
				    			<select><option>項目</option></select>
			    			</div>
				    	</dd>
					</dl>					

					<dl>
						<dt>職稱:</dt>
						<dd><input type="text" value="主管人員"></dd>
					</dl>					

					<dl>
						<dt>年收入:</dt>
						<dd>
							<select>
				    			<option>30-50萬</option>
				    		</select>
				    	</dd>
					</dl>					

					<dl>
						<dt>公司名稱:</dt>
						<dd><input type="text" value="品味新聞台"></dd>
					</dl>					

					<dl>
						<dt>公司電話:</dt>
						<dd><input type="text" value="(02)22668080"></dd>
					</dl>					

					<dl>
						<dt>他行帳戶驗證:</dt>
						<dd>808玉山銀行 <br>0679979002345</dd>
					</dl>

				</div>				

				<div class="formListCheck display-table">
					<dl>
						<dt>我不具備美國或其他國家/地區的納稅義務人身分</dt>
					</dl>								
					<dl>
						<dt>我同意將資料用於本行合作單位及<br>
						關係企業之行銷建檔、揭露、轉介或交互運用</dt>
					</dl>					

				</div>

				<div class="row btns">
					<div class="col-6 offset-3 text-center">
						<a class="btn-send" href="supplement-2">下一步</a>
					</div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

