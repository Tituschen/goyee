﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--kv-->
	<section class="bg kv product-kv">
		<div class="container">
			<h2>GOYEE產品介紹 <small>陪伴您灌溉人生夢想</small></h2>
		</div>
	</section>
	
    <!--product-->
	<section  class="productArea">
	        <div class="container">
		        <nav class="category variety">
	        		<ul class="inlineBlock">
	        			<li class="active"><a href="product">台外幣服務</a></li>
	        			<!--<li><a href="product-2">外幣服務</a></li>-->
	        			<li><a href="product-3">西聯服務</a></li>
						<li><a href="product-4">理財服務</a></li>
	        		</ul>
	        	</nav>
				
				<h1>台外幣服務</h1>
				<dl hidden="">
				   <dt><img src="../assets/images/product/icon.png"></dt>
				   <dd>
					   <h2>儲蓄起頭並不難 <span>活儲利率最高２%</span></h2>
					   <h3>超越一般活儲利率，讓你輕鬆儲蓄</h3>
				   </dd>		
				</dl>
				<dl>
				    <dt><img src="../assets/images/product/icon2.png"></dt>
					<dd>
					    <h2>多種轉帳超便利</h2>
					    <h3>行動守門員，安心轉帳不用怕<br>預約週期轉，不用擔心定期支出遲繳</h3>
					</dd>		
				</dl>
				<dl>
				   <dt><img src="../assets/images/product/icon3.png"></dt>
				   <dd>
					   <h2>每月享有 <span>免費6次</span></h2>
					   <h3>6次跨行提款免手續費<br>6次跨行轉帳免手續費<br></h3>
					   <p>詳細內容請參考注意事項</p>
				   </dd>		
				</dl>

				<dl hidden="">
				   <dt><img src="../assets/images/product/icon4.png"></dt>
					<dd>
					    <h2>超優利外幣定存</h2>
					    <h3>透過數位帳戶申購美元或人民幣.即享<span>外幣超優利率</span></h3>
						<table class="tb_collapsetb">
							  <tr>
									<td>幣別</td>
									<td>優惠利率</td>
									<td>活動期間</td>
							  </tr>
							  <tr>
									<td>美金</td>
									<td>2.5%</td>
									<td>即日起-2020.12.31</td>
							  </tr>
							  <tr>
									<td>人民幣</td>
									<td>2.5%</td>
									<td>即日起-2020.12.31</td>
							  </tr>	
                         </table>
					</dd>		
				</dl>
				<dl>
				   <dt><img src="../assets/images/product/icon5.png"></dt>
				   <dd>
					   <h2>線上換匯最划算<span>最高減５分</span></h2>
					   <h3> 24小時換會不停歇，省時又便利</h3>
					   <table class="tb_collapsetb">
							  <tr>
									<td>幣別</td>
									<td>優惠幅度</td>
									<td>幣別</td>
									<td>優惠幅度</td>  
							  </tr>
							  <tr>
									<td>美金</td>
									<td>-3分</td>
									<td>人民幣</td>
									<td>-1分</td>  
							  </tr>
							  <tr>
									<td>日幣</td>
									<td>-0.1分</td>
									<td>英鎊</td>
									<td>-5分</td>  
							  </tr>	
						   	  <tr>
									<td>加拿大幣</td>
									<td>-2分</td>
									<td>歐元</td>
									<td>-5分</td>  
							  </tr>	
						   	  <tr>
									<td>澳幣</td>
									<td>-2分</td>
									<td>南非幣</td>
									<td>-1分</td>  
							  </tr>	
						   	  <tr>
									<td>港幣</td>
									<td>-2分</td>
									<td></td>
									<td></td>  
							  </tr>	
					   </table>
					   <p>於本行營業時間09:00~15:30線上申購外幣，依各幣別牌告賣出匯率享有上表換匯優惠</p>
				   </dd>		
				</dl>
				
				<figure>
				  <div class="faqList display-table-xs-none">
					<dl class="">
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="text">
							<h4>注意事項</h4>
							<hr class="hr1">
                            <ul>
								<li>每月跨行提款/轉帳各6次免手續費。<br>優惠期間：自本行通知開戶成功日起至2020/12/31止。</li>
								<li>每月跨行提款/轉帳優惠次數不可跨月累積。</li>
								<li>跨行轉帳手續費優惠僅限以GOYEE數位帳戶於本行個人網路銀行/京城行動銀行App/國內ATM/網路ATM等通路執行轉帳交易時適用。</li>
								<li>跨行提款手續費優惠僅限以GOYEE數位帳戶於國內ATM執行提款交易時適用。</li>
								<li>如欲使用本行個人網路銀行/行動銀行App台幣非約定轉帳交易，需先
									<a class="underline" href="https://eatm.ktb.com.tw/KTBWebATM/indexAppDev.jsp">下載行動守門員App</a> 並完成綁定</li>
								<li>本行網路銀行外匯業務服務時間為24小時，惟辦理個人網路銀行及行動銀行外匯活期性存款與台幣活期性存款互轉，每筆交易最低限額為等值新台幣500元，且每人每一營業日累積最高限額(全行臨櫃交易及網銀交易合併計算)不得超過等值新台幣伍拾萬元(不含)。</li>
								<li>倘遇外匯市場波動劇烈時，本行得視實際情形需要，暫停相關外匯結匯之服務。</li>
								<li>承作外幣如涉及幣別轉換可能有匯兌損失，申請人應審慎考量評估，自行決定是否承作。</li>

                            </ul>
                            <hr>
						</dd>
					</dl>	
				</figure>
				
		     </div>
	</section>
	
	

</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		productToggle();
  	})
</script>

</body>
</html>

