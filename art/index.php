﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body class="index-fb">
<?php include("include/header.php"); ?>

<!--主版位-->
<main class="wrapper">
	
	
	<div class="imgbg boxbg01">
		<div class="container">	
			<aside><img src="../assets/images/index/plate01.png" alt=""/></aside>
			<article>
				<h1>GOYEE </h1>
                <h1>數位帳戶</h1>
				<h3>陪伴您灌溉人生夢想</h3>
			</article>
		</div>
	</div>
	
	<div class="imgbg boxbg02">
		<div class="container">	
			<ul class="vertical">
				 <li class="upright">PRESENT</li>
				 <li class="vrl">四步驟.一次滿足</li>
				 <li style="text-align:center;padding-left:5px;"><img src="../assets/images/index/figure.png" alt=""/></li>
			 </ul>
			 <article class="titlebox">			
				 <h1>開戶Hen容易！</h1>
                 <h2>向零存款說再見</h2>
				 <h2>儲蓄起頭並不難</h2>
			 </article>
			<aside><img src="../assets/images/index/plate02.png" alt=""/></aside>
		</div>
	</div>
	
	
	<div class="imgbg boxbg03">
		<div class="container">	
			 <aside><img src="../assets/images/index/plate03.gif" alt=""/></aside>
			 <article class="titlebox r-box">			
				 <h1>四個步驟，立即開戶</h1>
                 <h2>只要「四個步驟，一次滿足，三種需求」</h2>
				 <h2>就完成台外幣開戶。</h2>
			 </article>
		</div>
	</div>
	
	
	<div class="imgbg boxbg04">
		<div class="container">	
		  <ul class="vertical">
				 <li class="upright">PRESENT</li>
				 <li class="vrl">西聯匯款</li>
				 <li style="text-align:center"><img src="../assets/images/index/figure.png" alt=""/></li>
			 </ul>
			 <aside><img src="../assets/images/index/plate04.gif" alt=""/></aside>
			 <article class="titlebox r-box">			
				 <h1>換匯Hen便利！</h1>
                 <h2>多種幣別任你選，線上換匯夜未眠</h2>
				 <h2>全日不停歇，指定時間換匯</h2>
				 <h3>再享優惠美元<b>3</b>分</h3>
				 <a href="product-2">Read more</a>
			 </article>
		</div>
	</div>
	
	<div class="imgbg boxbg05">
		<div class="container">	
		  <ul class="vertical">
				 <li class="upright">PRESENT</li>
				 <li class="vrl">西聯匯款</li>
				 <li style="text-align:center;padding-left:5px;"><img src="../assets/images/index/figure.png" alt=""/></li>
			 </ul>
			 <aside><img src="../assets/images/index/plate05.gif" alt=""/></aside>
			 <article class="titlebox l-box">			
				 <h1>收款Hen快速！</h1>
                 <h2><i><img src="../assets/images/index/logo01.png" alt=""/></i>跨國收款免出門</h2>
				 <h2>領取<i><img src="../assets/images/index/logo02.png" alt=""/></i>廣告收入!</h2>
				<a href="product-3">Read more</a>
			 </article>
		</div>
	</div>
	
	<div class="imgbg boxbg06">
		<div class="container">	
			<ul class="vertical">
				 <li class="upright">PRESENT</li>
				 <li class="vrl">基金優惠</li>
				 <li style="text-align:center;padding-left:5px;"><img src="../assets/images/index/figure.png" alt=""/></li>
			 </ul>
			 <aside><img src="../assets/images/index/plate06.gif" alt=""/></aside>
			 <article class="titlebox r-box">			
				 <h1>理財Hen簡單！</h1>
                 <h2>投資理財好簡單，<br>京選基金手續費<b>4</b><span>折</span></h2>
				 <a href="product-4">Read more</a>
			 </article>
			
			 
		</div>
	</div>	
			
	<div class="imgbg boxbg08">
		<div class="container">	
			 <ul class="vertical">
				 <li class="upright">PRESENT</li>
				 <li class="vrl">申辦流程</li>
				 <li style="text-align:center;padding-left:5px;"><img src="../assets/images/index/figure.png" alt=""/></li>
			 </ul>
			 <aside><img src="../assets/images/index/plate07.gif" alt=""/></aside>
			 <article class="titlebox">			
				 <h1>快速申請開戶</h1>
                 <h1>Hen方便</h1>
			</article>		 
		</div>
	</div>	

	<div class="footertopbg">
		 <aside></aside>
	</div>	
		
	
	<!--btnlink-->
	<nav class="btnlink">
		<a href="establishment">
			<img src="../assets/images/index/openbtn.png">
		</a>
	</nav>	

</main>
<!--主版位End-->
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

 <!--TweenMax-->
<script src="../assets/js/TweenMax.min.js"></script>
<script src="../assets/js/jquery.superscrollorama.min.js"></script>
<script src="../assets/js/jquery.mousewheel.js"></script>
<script src="../assets/js/jquery.transit.min.js"></script>

<script src="../assets/js/movie.js"></script>

<script>
  	$(function(){
  		indexMoving();
  	})
</script>


</body>
</html>