﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li>
					<h4>01.</h4>
					<p>上傳您的資料</p>
				</li>				

				<li>
					<h4>02.</h4>
					<p>填寫個人資料</p>
				</li>				

				<li>
					<h4>03.</h4>
					<p>填寫申請資料</p>
				</li>				

				<li class="active">
					<h4>04</h4>
					<p>確認資料</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document form">
	        <div class="container">

	            <h1>請您確認個人資料</h1>
				
				<div class="formListCheck display-table">
					<dl>
						<dt>中文姓名:</dt>
						<dd>宋瞧安</dd>
					</dl>					

					<dl>
						<dt>身分證字號:</dt>
						<dd>A223456789</dd>
					</dl>					


					<dl>
						<dt>手機號碼:</dt>
						<dd>0988777666</dd>
					</dl>					

					<dl>
						<dt>出生日期:</dt>
						<dd>69/5/23</dd>
					</dl>					

					<dl>
						<dt>發證日期:</dt>
						<dd>100/12/31</dd>
					</dl>
				</div>				

				<div class="formListCheck display-table">
					<dl>
						<dt>英文姓名:</dt>
						<dd>SUNG CHIAO AN</dd>
					</dl>					

					<dl>
						<dt>開戶目的:</dt>
						<dd>投資理財</dd>
					</dl>					

					<dl>
						<dt>行業:</dt>
						<dd>新聞媒體業</dd>
					</dl>					

					<dl>
						<dt>職稱:</dt>
						<dd>主管人員</dd>
					</dl>					

					<dl>
						<dt>年收入:</dt>
						<dd>101~300萬</dd>
					</dl>					

					<dl>
						<dt>公司名稱:</dt>
						<dd>品味新聞台</dd>
					</dl>					

					<dl>
						<dt>公司電話:</dt>
						<dd>(02)22668080-333</dd>
					</dl>					

					<dl>
						<dt>他行帳戶驗證:</dt>
						<dd>808玉山銀行 <br>0679979002345</dd>
					</dl>

				</div>				

				<div class="formListCheck display-table">
					<dl>
						<dt>我不具備美國或其他國家/地區的納稅義務人身分</dt>
					</dl>								
					<dl>
						<dt>我同意將資料用於本行合作單位及<br>
						關係企業之行銷建檔、揭露、轉介或交互運用</dt>
					</dl>					

				</div>

				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="establishment-6">上一步</a></div>
					<div class="col-6"><a class="btn-send" href="establishment-8">確認</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){

  	})
</script>

</body>
</html>

