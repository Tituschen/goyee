﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList">
		<div class="container">
			<ul class="inlineBlock">
				<li class="active">
					<h4>01.</h4>
					<p>上傳您的資料</p>
				</li>				

				<li>
					<h4>02.</h4>
					<p>填寫個人資料</p>
				</li>				

				<li>
					<h4>03.</h4>
					<p>填寫申請資料</p>
				</li>				

				<li>
					<h4>04</h4>
					<p>確認資料</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document form">
	        <div class="container">

	            <h1>請上傳文件確認您的身分</h1>
				
				<div class="row formList">

					<div class="col-sm-6 img">
						<h3 class="org">Step1：上傳身分證照片</h3>
						<p>請上傳您的身分證照片正面</p>

						<aside class="flex-center">
							<input type="file" name="">
							<div>
								<img src="../assets/images/ico/camera.svg">
								<p>身分證正面</p>
							</div>
						</aside>
						
						<!--
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
						-->
				    </div>

				    <div class="col-sm-6 txt">
				    	<ul>
				    		<li>
				    			<h5>中文姓名</h5>
				    			<input type="text"  placeholder="請輸入" name="">
				    		</li>				    		

				    		<li>
				    			<h5>身分證字號</h5>
				    			<input type="text"  placeholder="請輸入" name="" value="A223456789" disabled="">
				    		</li>				    		

				    		<li>
				    			<h5>出生日期(民國)</h5>
				    			<div class="clip clip-3">
					    			<input type="text"  placeholder="YYY" name="" class="text-center">
					    			<input type="text"  placeholder="MM" name=""  class="text-center">
					    			<input type="text"  placeholder="DD" name=""  class="text-center">
				    			</div>
				    		</li>				    		

				    		<li>
				    			<h5>發證日期(民國)</h5>
				    			<div class="clip clip-3">
					    			<input type="text"  placeholder="YYY" name="" class="text-center">
					    			<input type="text"  placeholder="MM" name=""  class="text-center">
					    			<input type="text"  placeholder="DD" name=""  class="text-center">
				    			</div>
				    		</li>				    		

				    		<li>
				    			<h5>發證地點</h5>
				    			<div class="clip clip-2">
					    			<select><option>北市</option></select>
					    			<select><option>煥發</option></select>
				    			</div>
				    		</li>

				    	</ul>
				    </div>
				</div>

				<hr>

				<div class="row formList">

					<div class="col-sm-6 img">
						<p>請上傳您的身分證照片背面</p>
						<aside class="flex-center">
							<input type="file" name="">
							<div>
								<img src="../assets/images/ico/camera.svg">
								<p>身分證背面</p>
							</div>
						</aside>
						
						<!--
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
						-->
				    </div>

				    <div class="col-sm-6 txt">
				    	<ul>

				    		<li>
				    			<h5>戶籍地址</h5>
				    			<div class="clip clip-2">
					    			<select><option>台北市</option></select>
					    			<select><option>大安區</option></select>
				    			</div>
				    			<input type="text"  placeholder="請輸入地址" name="" class="address">
				    		</li>				    		

				    		<li>
				    			<h5>
				    				<span>通訊地址-是否同戶籍地址？</span>
				    				<label>
				    					<input type="radio" name="aa" checked=""><span>是</span>
				    					<input type="radio" name="aa"><span>否</span>
				    				</label>
				    			</h5>
				    			<div class="clip clip-2">
					    			<select><option>台北市</option></select>
					    			<select><option>大安區</option></select>
				    			</div>
				    			<input type="text"  placeholder="請輸入地址" name="" class="address">
				    			<p>(金融卡將寄送通訊地址)</p>
				    		</li>

				    	</ul>
				    </div>
				</div>

				<hr>

				<div class="row formList">

					<div class="col-md-6 offset-md-3  img">

						<h3 class="org">Step2：上傳第二身分證照片</h3>
						<p>上傳可辨識您身分的第二證件　例如：健保卡．駕照</p>

						<aside class="flex-center">
							<input type="file" name="">
							<div>
								<img src="../assets/images/ico/camera.svg">
								<p>第二證件<small>(健保卡或駕照)</small></p>
							</div>
						</aside>
						
						<!--
						<p>拍攝時提示：</p>
						<ol>
							<li>請縮放照片以符合裁切範圍，不要切到證件</li>
							<li>目前拍攝正面/背面/第二證件</li>
						</ol>
						-->
				    </div>

				</div>

				<div class="row btns">
				    <div class="col-6"><a class="btn-send" href="establishment-3">上一步</a></div>
					<div class="col-6"><a class="btn-send" href="establishment-5">下一步</a></div>
				</div>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

