﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">
	


	<section class="bg kv debit-kv">
		<div class="container">
			<h2>GOYEE數位帳戶服務開通<small>陪伴您灌溉人生夢想</small></h2>
		</div>
	</section>
	
	<!--Interrogation-->
	<section class="Interrogation debitInter">
	        <div class="container">
			  <h1><b>Welcome to GOYEE!</b></h1>
			  <h2>請選擇您要開通的服務</h2>
				<ul class="inlineBlock">
				   <li>
				   		<a href="debit-open-2">
						   <aside><img src="../assets/images/debit/img1.jpg" alt=""/></aside>
						   <h4>我要開通金融卡</h4>
						</a>
				   </li>
				   <li>
				   		<a href="webbank-open-2">
						   <aside><img src="../assets/images/debit/img2.jpg" alt=""/></aside>
						   <h4>我要開通網路銀行</h4>
						</a>
				   </li>	
				</ul>
	        </div><!--containerEND-->
	</section>
	
</main>
<!--主版位End-->

<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){

  	})
</script>


</body>
</html>

