﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
		
	
<!--主版位-->
<main class="wrapper">
	
    <!--stepList-->
    <div class="stepList stepList-3">
		<div class="container">
			<ul class="inlineBlock">
				<li >
					<h4>01.</h4>
					<p>更新資料</p>
				</li>										

				<li >
					<h4>02.</h4>
					<p>確認資料</p>
				</li>				

				<li class="active"> 
					<h4>03.</h4>
					<p>修改成功</p>
				</li>

			</ul>
		</div>
    </div>
	
	<!--document-->
	<section class="document endbox">

        <div class="container">
            
            <h1>您已完成補件資料上傳!!</h1>
			<article>
                <h2>請至您的email確認送件流程</h2>
				<h2>並靜候1~2個營業日</h2>
				<h2>我們會以email及簡訊方式通知您開戶結果</h2>
			</article>

			<div class="row  btns">
				<div class="col-6 offset-3 text-center">
					<a class="btn-send" href="index">回首頁</a>
				</div>
			</div>	
	
	 	</div><!--containerEND-->
	</section>

</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){})
</script>

</body>
</html>

