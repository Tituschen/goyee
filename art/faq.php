﻿<!DOCTYPE html>
<html>
<head>
<?php include("include/meta.php"); ?>
</head>
<body>
<?php include("include/header.php"); ?>
	
<!--主版位-->
<main class="wrapper">

	<!--news-->
	<section class="faqArea">
	        <div class="container">
	        	
	        	<nav class="category">
	        		<ul class="inlineBlock">
	        			<li class="active"><a href="#">帳戶相關</a></li>
	        			<li><a href="#">轉帳相關</a></li>
	        			<li><a href="#">交易相關</a></li>
	        			<li><a href="#">其他</a></li>
	        		</ul>
	        	</nav>

	        	<nav class="category category-sub">
	        		<ul class="inlineBlock">
	        			<li ><a href="#">數位帳戶</a></li>
	        			<li><a href="#">開戶驗證</a></li>
	        			<li class="active"><a href="#">代號密碼</a></li>
	        			<li><a href="#">金融卡</a></li>
	        		</ul>
	        	</nav>

				<div class="faqList display-table display-table-xs-none">

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q1.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q2.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q3.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q4.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>						

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q5.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q6.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q7.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q8.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>						

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q9.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>					

					<dl>
						<dt><a href="#" class="btn-qa-toggle"></a></dt>
						<dd class="title">Q10.</dd>
						<dd class="text">
							<h4>Lorem Ipsum is simply dummy text  of the printing and typesetting industry?</h4>
							<p >Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</dd>
					</dl>						

				</div>

				<!--pagination-->
				<nav aria-label="Page navigation">
				  <ul class="pagination justify-content-center">
				    <li class="page-item">
				    	<a class="page-link page-link-prev" href="#"><img src="../assets/images/ico/arrow-left.png">Prev</a>
				    </li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item"><a class="page-link" href="#">4</a></li>
				    <li class="page-item"><a class="page-link" href="#">5</a></li>
				    <li class="page-item">
				    	<a class="page-link  page-link-next" href="#">Next<img src="../assets/images/ico/arrow-left.png"></a>
				    </li>
				  </ul>
				</nav>

		    </div><!--containerEND-->
	</section>	
</main>
<!--主版位End-->
	
<?php include("include/footer.php"); ?>
<?php include("include/js.php"); ?>

<script>
  	$(function(){
  		faqToggle();
  	})
</script>

</body>
</html>

